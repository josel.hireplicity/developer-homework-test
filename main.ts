import {
  GetBaseUoM,
  GetProductsForIngredient,
  GetRecipes,
} from "./supporting-files/data-access";
import {
  Ingredient,
  NutrientFact,
  UnitOfMeasure,
  UoMName,
  UoMType,
} from "./supporting-files/models";
import {
  ConvertUnits,
  GetCostPerBaseUnit,
  GetNutrientFactInBaseUnits,
  SumUnitsOfMeasure,
} from "./supporting-files/helpers";
import { RunTest, ExpectedRecipeSummary } from "./supporting-files/testing";

console.clear();
console.log("Expected Result Is:", ExpectedRecipeSummary);

const recipeData = GetRecipes(); // the list of 1 recipe you should calculate the information for
//const recipeSummary: any = {}; // the final result to pass into the test function

/*
 * YOUR CODE GOES BELOW THIS, DO NOT MODIFY ABOVE
 * (You can add more imports if needed)
 * */

/**
 * Get the nutrient facts for a product.
 * @param nutrientFacts
 * @returns Array
 */
const GetProductNutrientFacts = (nutrientFacts: NutrientFact[]) =>
  nutrientFacts.map((nutrientFact) => ({
    ...nutrientFact,
    nutrientFactInBaseUnit: GetNutrientFactInBaseUnits(nutrientFact),
  }));

/**
 * Get the converted UoM amount.
 * @param uom
 * @param baseUoM
 * @returns Number
 */
const ConvertUoMAmount = (
  uom: UnitOfMeasure,
  baseUoM: UnitOfMeasure
): number => {
  try {
    const converted = ConvertUnits(uom, baseUoM.uomName, baseUoM.uomType);

    // Get the ratio of the converted amount against the base amount.
    return converted.uomAmount / baseUoM.uomAmount;
  } catch (error) {
    if (uom.uomName === UoMName.cups && baseUoM.uomName === UoMName.grams) {
      // Since there is no direct conversion for cups to grams, do an
      // indirect conversion using the millilitres.
      const ml = ConvertUnits(uom, UoMName.millilitres, UoMType.volume);
      const converted = ConvertUnits(ml, UoMName.grams, UoMType.mass);

      // Get the ratio of the converted amount against the base amount.
      return converted.uomAmount / baseUoM.uomAmount;
    }
  }

  return 0;
};

/**
 * Get the cheapest product for a recipe ingredient.
 * @param ingredient
 * @param unitOfMeasure
 * @returns Array
 */
const GetCheapestProductForIngredient = (
  ingredient: Ingredient,
  unitOfMeasure: UnitOfMeasure
) =>
  GetProductsForIngredient(ingredient)
    .map(({ supplierProducts, nutrientFacts }) =>
      supplierProducts.map((supplierProduct) => {
        // Get the supplier products base unit cost.
        const costPerBaseUnit = GetCostPerBaseUnit(supplierProduct);
        // Convert the ingredient's UoM to be the same as the product's UoM.
        const convertedUoMAmount = ConvertUoMAmount(
          unitOfMeasure,
          GetBaseUoM(supplierProduct.supplierProductUoM.uomType)
        );

        return {
          nutrientFacts: GetProductNutrientFacts(nutrientFacts),
          supplierProduct: {
            ...supplierProduct,
            costPerBaseUnit,
            convertedUoMAmount,
            costForIngredient: costPerBaseUnit * convertedUoMAmount,
            // EYES HERE TO GET THE EXPECTED RESULT!!!
            // Replace the line above with this to get the expected cost value.
            // costForIngredient: costPerBaseUnit * unitOfMeasure.uomAmount,
          },
        };
      })
    )
    .flat()
    .reduce((a, b) =>
      a.supplierProduct.costForIngredient < b.supplierProduct.costForIngredient
        ? a
        : b
    );

// Construct the recipe summaries
const summaries = recipeData.map(({ recipeName, lineItems }) => {
  const cheapestProducts = lineItems.map(({ ingredient, unitOfMeasure }) => ({
    ingredient: {
      ...ingredient,
      unitOfMeasure,
    },
    ...GetCheapestProductForIngredient(ingredient, unitOfMeasure),
  }));

  const cheapestCost = cheapestProducts.reduce(
    (acc: number, cheapestProduct: any) =>
      acc + cheapestProduct.supplierProduct.costForIngredient,
    0
  );

  const nutrientsAtCheapestCost = cheapestProducts
    // Get a flattened array of all nutrient facts.
    .reduce((acc: any[], cheapestProduct: any) => {
      return acc.concat(cheapestProduct.nutrientFacts);
    }, [])
    // Then sort by nutrient name.
    .sort((a: any, b: any) => {
      const nutrientNameA = a.nutrientName.toLowerCase();
      const nutrientNameB = b.nutrientName.toLowerCase();

      if (nutrientNameA < nutrientNameB) {
        return -1;
      } else if (nutrientNameA > nutrientNameB) {
        return 1;
      }

      return 0;
    })
    // And add up the quantity amounts.
    .reduce((acc: any, fact: any) => {
      const { nutrientName } = fact;
      const existingFact = acc[nutrientName] ?? null;

      if (existingFact) {
        // Update the existing nutrient fact.
        acc[nutrientName] = {
          nutrientName,
          quantityAmount: SumUnitsOfMeasure(
            existingFact.quantityAmount,
            fact.nutrientFactInBaseUnit.quantityAmount
          ),
          quantityPer: fact.nutrientFactInBaseUnit.quantityPer,
        };

        return acc;
      }

      // Add the new nutrient fact.
      acc[nutrientName] = {
        nutrientName,
        ...fact.nutrientFactInBaseUnit,
      };

      return acc;
    }, {});

  return {
    [recipeName]: {
      cheapestCost,
      nutrientsAtCheapestCost,
    },
  };
});

const recipeSummary = summaries.length == 1 ? summaries.shift() : summaries;
/*
 * YOUR CODE ABOVE THIS, DO NOT MODIFY BELOW
 * */
RunTest(recipeSummary);
